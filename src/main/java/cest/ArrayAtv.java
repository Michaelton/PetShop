package cest;

public class ArrayAtv {

	public static void main(String[] args) {

		String testeArray1[] = { "A1", "B1", "C1" };
		String testeArray2[] = { "A2", "B2", "C2", "D2", "E2" };

		CompArray arrayComp = new CompArray();

		System.out.println("\n1° Teste da comparação das Arrays: ");

		arrayComp.setArray1(testeArray1, testeArray2);
		arrayComp.getComp();

		System.out.println("\n2° Teste da comparação das Arrays: ");

		testeArray1 = testeArray2;

		arrayComp.setArray1(testeArray1, testeArray2);
		arrayComp.getComp();

		AtvArrayList arrayListaAtv = new AtvArrayList();

		System.out.println("\n\nTeste das Arrays Lists:\n");

		arrayListaAtv.setLista1("Texto Lista 1");
		arrayListaAtv.setLista1(11111);
		arrayListaAtv.setLista1("Texto2 Lista 1");

		System.out.println("Quantidade Atual de elementos na Lista 1: " + arrayListaAtv.sizeLista1());

		arrayListaAtv.removeLista1(1);

		System.out.println(
				"Quantidade de elementos na Lista 1 após a remoção de elemento: " + arrayListaAtv.sizeLista1());

		arrayListaAtv.setLista2("Texto Lista 2");
		arrayListaAtv.setLista2(22222);
		arrayListaAtv.setLista2("Texto2 Lista 2");

		System.out.println("\nQuantidade Atual de elementos na Lista 2: " + arrayListaAtv.sizeLista2());

		arrayListaAtv.removeLista2(0);
		arrayListaAtv.removeLista2(1);

		System.out.println(
				"Quantidade de elementos na Lista 2 após a remoção de elementos: " + arrayListaAtv.sizeLista2());

	}

}
