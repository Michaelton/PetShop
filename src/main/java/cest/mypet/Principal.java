package cest.mypet;

import cest.mypet.cadastro.Animal;
import cest.mypet.cadastro.Pessoa;

public class Principal {

	public static void main(String[] args) {

		System.out.println("\nInicio do Teste Encapsulamento de Classe\n");

		System.out.println("\nInico do Teste da Classe Animal\n");

		Animal testeAnimal = new Animal();

		testeAnimal.setNome("NomeAnimal");
		testeAnimal.setIdade(000);
		testeAnimal.setTipo(00000, "DescriçãoAnimal");

		System.out.println("Nome do Animal: " + testeAnimal.getNome());
		System.out.println("Idade do Animal: " + testeAnimal.getIdade());
		System.out.println("Tipo do Animal:\n\t\tCódigo: " + testeAnimal.getTipoCod() + "\n\t\tDescricao: "
				+ testeAnimal.getTipoDescricao());

		System.out.println("\nFim do Teste da Classe Animal");

		System.out.println("\nInico do Teste da Classe Pessoa\n");

		Pessoa testePessoa = new Pessoa();

		testePessoa.setNome("NomePessoa");
		testePessoa.setIdade(111);
		testePessoa.setCidade("CódigoCidade", "DescrçãoCidade", "NomeCidade");

		System.out.println("Nome da Pessoa: " + testePessoa.getNome());
		System.out.println("Idade da Pessoa: " + testePessoa.getIdade());
		System.out.println("Cidade:\n\tUF:\n\t\tCódigo: " + testePessoa.getCidadeUFCod() + "\n\t\tDescrição: "
				+ testePessoa.getCidadeUFDescricao() + "\n\t\tNome: " + testePessoa.getCidadeNome());

		System.out.println("\nFim do Teste da Classe Pessoa");

		System.out.println("\nFim do Teste Encapsulamento de Classe\n");
	}

}
