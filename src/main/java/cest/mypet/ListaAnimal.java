package cest.mypet;

import java.util.ArrayList;
import cest.mypet.cadastro.Animal;
import cest.mypet.cadastro.Pessoa;

public class ListaAnimal {

	public static void main(String[] args) {

		ArrayList<Animal> listaAnimal = new ArrayList<Animal>();

		Animal animal1 = new Animal();
		animal1.setNome("Nome Animal 1");

		Animal animal2 = new Animal();
		animal2.setNome("Nome Animal 2");

		Animal animal3 = new Animal();
		animal3.setNome("Nome Animal 3");
		
		Pessoa pessoa1 = new Pessoa();
		pessoa1.setNome("Nome Pessoa 1");

		listaAnimal.add(animal1);
		listaAnimal.add(animal2);
		listaAnimal.add(animal3);
		
		
		System.out.println(" ");

		for (Animal lista:listaAnimal) {
			System.out.println("Nome do Animal: "+lista.getNome());
		}
	}

}
