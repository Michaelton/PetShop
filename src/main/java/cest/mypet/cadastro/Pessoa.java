package cest.mypet.cadastro;

import cest.mypet.cadastro.loc.Cidade;

public class Pessoa {

	private String nome;
	private int idade;
	private Cidade cidade = new Cidade();

	public void setNome(String nomeR) {
		this.nome = nomeR;
	}

	public String getNome() {
		return nome;
	}

	public void setIdade(int idadeR) {
		this.idade = idadeR;
	}

	public int getIdade() {
		return idade;
	}

	public void setCidade(String ufCodR, String ufDescricaoR, String nomeR) {
		this.cidade.setUF(ufCodR, ufDescricaoR);
		this.cidade.setNome(nomeR);
	}

	public String getCidadeUFCod() {
		return cidade.getUFCod();
	}

	public String getCidadeUFDescricao() {
		return cidade.getUFDescricao();
	}

	public String getCidadeNome() {
		return cidade.getNome();
	}
}
