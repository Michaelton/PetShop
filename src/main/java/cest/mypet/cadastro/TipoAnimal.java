package cest.mypet.cadastro;

public class TipoAnimal {

	private int cod;
	private String descricao;

	public void setCod(int codR) {
		this.cod = codR;
	}

	public int getCod() {
		return cod;
	}

	public void setDescricao(String descricaoR) {
		this.descricao = descricaoR;
	}

	public String getDescricao() {
		return descricao;
	}

}
