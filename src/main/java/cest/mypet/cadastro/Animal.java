package cest.mypet.cadastro;

public class Animal {

	private String nome;
	private int idade;
	private TipoAnimal tipo = new TipoAnimal();

	public void setNome(String nomeRecebido) {
		this.nome = nomeRecebido;
	}

	public String getNome() {
		return nome;
	}

	public void setIdade(int idadeR) {
		this.idade = idadeR;
	}

	public int getIdade() {
		return idade;
	}

	public void setTipo(int tipoCodR, String tipoDescricaoR) {
		this.tipo.setCod(tipoCodR);
		this.tipo.setDescricao(tipoDescricaoR);
	}

	public int getTipoCod() {
		return tipo.getCod();
	}

	public  String getTipoDescricao() {
		return tipo.getDescricao();
	}


}
