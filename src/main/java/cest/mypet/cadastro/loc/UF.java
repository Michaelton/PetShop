package cest.mypet.cadastro.loc;

public class UF {

	private String cod;
	private String descricao;

	public void setCod(String codR) {
		this.cod = codR;
	}

	public String getCod() {
		return cod;
	}

	public void setDescricao(String descricaoR) {
		this.descricao = descricaoR;
	}

	public String getDescricao() {
		return descricao;
	}
}
