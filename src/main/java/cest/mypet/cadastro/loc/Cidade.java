package cest.mypet.cadastro.loc;

public class Cidade {

	private UF uf = new UF();
	private String nome;

	public void setUF(String codR, String descricaoR) {
		this.uf.setCod(codR);
		this.uf.setDescricao(descricaoR);
	}

	public String getUFCod() {
		return uf.getCod();
	}

	public String getUFDescricao() {
		return uf.getDescricao();
	}

	public void setNome(String nomeR) {
		this.nome = nomeR;
	}

	public String getNome() {
		return nome;
	}
}
