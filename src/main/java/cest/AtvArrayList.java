package cest;

import java.util.ArrayList;

public class AtvArrayList {

	private ArrayList lista1 = new ArrayList();
	private ArrayList lista2 = new ArrayList();

	public void setLista1(Object addR) {
		this.lista1.add(addR);
	}

	public void setLista2(Object addR) {
		this.lista2.add(addR);
	}

	public void removeLista1(int index) {
		this.lista1.remove(index);
	}

	public void removeLista2(int index) {
		this.lista2.remove(index);
	}

	public int sizeLista1() {
		return this.lista1.size();
	}

	public int sizeLista2() {
		return this.lista2.size();
	}

}
